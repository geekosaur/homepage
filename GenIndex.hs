import Data.List
import Data.Maybe
import System.Environment
import Text.Show.Pretty

import System.Directory
import System.FilePath
import Hakyll
import Types

collectFiles :: FilePath -> IO [DownloadFile]
collectFiles root = do
    getRecursiveContents (return . isHidden) root >>= fmap catMaybes . mapM toDownloadFile
  where
    isHidden path = "." `isPrefixOf` takeFileName path
    isTarball name = ".tar.gz"  `isSuffixOf` name
                  || ".tar.bz2" `isSuffixOf` name
                  || ".tar.xz"  `isSuffixOf` name
    toDownloadFile path
      | isTarball path = do
        let version = last $ splitDirectories root
        let sigPath = path <.> "sig"

        sig <- doesFileExist (root </> sigPath)
        size <- getFileSize (root </> path)
        return $ Just $ DownloadFile { filePath = version </> path
                                     , fileSize = size
                                     , fileSignature = if sig then Just (version </> sigPath) else Nothing
                                     }
      | otherwise = return Nothing

main :: IO ()
main = do
    [root] <- getArgs
    files <- collectFiles root
    putStrLn (ppShow files)
