---
author: Zubin Duggal
title: "GHC 9.4.5 is now available"
date: 2023-04-18
tags: release
---

The GHC developers are happy to announce the availability of GHC 9.4.5. Binary
distributions, source distributions, and documentation are available at
[downloads.haskell.org](https://downloads.haskell.org/ghc/9.4.5).

This release is primarily a bugfix release addressing a few issues
found in 9.4.4. These include:

 * Fixes for a number of bug fixes in the simplifier (#22623, #22718, #22913, 22695,
   #23184, #22998, #22662, #22725).
 * Many bug fixes to the non-moving and parallel GCs (#22264, #22327, #22926,
   #22927, #22929, #22930, #17574, #21840, #22528)
 * A fix a bug with the alignment of RTS data structures that could result in
   segfaults when compiled with high optimisation settings on certain platforms
   (#22975 , #22965).
 * Bumping `gmp-tarballs` to a version which doesn't use the reserved `x18`
   register on AArch64/Darwin systems, and also has fixes for CVE-2021-43618
   (#22497, #22789).
 * A number of improvements to recompilation avoidance with multiple home units
   (#22675, #22677, #22669, #22678, #22679, #22680)
 * Fixes for regressions in the typechecker and constraint solver (#22647,
   #23134, #22516, #22743)
 * Easier installation of binary distribution on MacOS platforms by changing the
   installation Makefile to remove the quarantine attribute when installing.
 * ... and many more. See the [release notes] for a full accounting.

As some of the fixed issues do affect correctness users are encouraged to
upgrade promptly.

We would like to thank Microsoft Azure, GitHub, IOG, the Zw3rk stake pool,
Well-Typed, Tweag I/O, Serokell, Equinix, SimSpace, Haskell Foundation, and
other anonymous contributors whose on-going financial and in-kind support has
facilitated GHC maintenance and release management over the years. Finally,
this release would not have been possible without the hundreds of open-source
contributors whose work comprise this release.

As always, do give this release a try and open a [ticket][] if you see
anything amiss.

Happy compiling,

- Zubin

[ticket]: https://gitlab.haskell.org/ghc/ghc/-/issues/new
[release notes]: https://downloads.haskell.org/~ghc/9.4.5/docs/users_guide/9.4.5-notes.html
