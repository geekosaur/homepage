---
author: bgamari
title: "GHC 8.10.1 released"
date: 2020-03-24
tags: release
---

The GHC team is happy to announce the availability of GHC 8.10.1. Source
and binary distributions are available at the [usual
place](https://downloads.haskell.org/ghc/8.10.1/).

GHC 8.10.1 brings a number of new features including:

 * The new `UnliftedNewtypes` extension allowing newtypes around unlifted
   types.

 * The new `StandaloneKindSignatures` extension allows users to give
   top-level kind signatures to type, type family, and class
   declarations.

 * A new warning, `-Wderiving-defaults`, to draw attention to ambiguous
   deriving clauses

 * A number of improvements in code generation, including a new loop analyzer, optimisation of `memset`, `memcpy` and array allocation, more aggressive specialisation, and pointer tagging for larger data types.
 
 * A new GHCi command, `:instances`, for listing the class instances
   available for a type.

 * An upgraded Windows toolchain lifting the `MAX_PATH` limitation

 * A new, low-latency garbage collector.

 * Improved support profiling, including support for sending profiler
   samples to the eventlog, allowing correlation between the profile and
   other program events

A full accounting of the changes of this release can be found in the [release notes](https://downloads.haskell.org/ghc/8.10.1/docs/html/users_guide/8.10.1-notes.html).
A guide for migrating existing code to GHC 8.10 can be found in the [release migration notes](https://gitlab.haskell.org/ghc/ghc/-/wikis/migration/8.10).

Note that at the moment we still require that macOS Catalina users
exempt the binary distribution from the notarization requirement by
running `xattr -cr .` on the unpacked tree before running `make install`.
This situation will hopefully be improved for GHC 8.10.2 with the
resolution of #17418.

Cheers,

- Ben

