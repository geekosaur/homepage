---
author: bgamari
title: "GHC 9.6.1-rc1 is now available"
date: 2023-03-03
tags: release
---

The GHC team is very pleased to announce the availability of the first (and
likely final) release candidate of GHC 9.6.1.
As usual, binaries and source distributions are available at
[downloads.haskell.org](https://downloads.haskell.org/ghc/9.6.1-rc1/).

Beginning with GHC 9.6.1, GHC can be built as a cross-compiler to
WebAssembly and JavaScript. This is an important step towards robust
support for compiling Haskell to the Web, but there are a few caveats to be
aware of in the 9.6 series:

 - Both the Javascript and WebAssembly backends are still at an early
   stage of development and are present in this release as a technology preview

 - Using GHC as a cross-compiler is not as easy as we would like it to
   be; in particular, there are challenges related to Template Haskell

 - GHC is not yet run-time retargetable; a given GHC binary targets
   exactly one platform, and both WebAssembly and JavaScript are considered
   platforms for this purpose. Cross-compilers must be built from source by
   their users

We hope to lift all of these limitations in future releases.

Additionally, 9.6.1 will include:

 - Significant latency improvements in the non-moving garbage collector

 - Efficient runtime support for delimited continuations

 - Improvements in compiler error messages

 - Numerous improvements in the compiler's memory usage

See the [release notes][] for a comprehensive accounting of changes in this
release.

As always, one can find a migration guide to aid in transitioning from older
releases on the [GHC Wiki][migration-guide]. We have also recently started
extending our release process to cover a wider set of Linux distributions. In
particular, we now offer Rocky 8 and Ubuntu 20.04 binary distributions which
cover RedHat-derivative and distributions using older `glibc` releases (namely
2.27), respectively.

Please do give this release a try and open a [ticket][] if you see anything
amiss. If all goes well we expect the final release should be available by late
next week.

Cheers,

- Ben


[ticket]: https://gitlab.haskell.org/ghc/ghc/issues/
[migration-guide]: https://gitlab.haskell.org/ghc/ghc/-/wikis/migration/9.6
[release notes]: https://downloads.haskell.org/ghc/9.6.1-rc1/docs/users_guide/9.6.1-notes.html
