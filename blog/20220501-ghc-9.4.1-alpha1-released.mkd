---
author: bgamari
title: "GHC 9.4.1-alpha1 released"
date: 2022-05-01
tags: release
---

The GHC developers are happy to announce the availability of the first
alpha release of the GHC 9.4 series. Binary distributions, source
distributions, and documentation are available at 
[downloads.haskell.org](https://downloads.haskell.org/ghc/9.4.1-alpha1).

This major release will include:

 - A new profiling mode, `-fprof-late`, which adds automatic cost-center
   annotations to all top-level functions *after* Core optimisation has
   run. This incurs significantly less performance cost while still
   providing informative profiles.

 - A variety of plugin improvements including the introduction of a new
   plugin type, *defaulting plugins*, and the ability for typechecking
   plugins to rewrite type-families.

 - An improved constructed product result analysis, allowing unboxing of
   nested structures, and a new boxity analysis, leading to less reboxing.

 - Introduction of a tag-check elision optimisation, bringing
   significant performance improvements in strict programs.

 - Generalisation of a variety of primitive types to be levity
   polymorphic. Consequently, the `ArrayArray#` type can at long last be
   retired, replaced by standard `Array#`.

 - Introduction of the `\cases` syntax from [GHC proposal 0302]

 - A complete overhaul of GHC's Windows support. This includes a
   migration to a fully Clang-based C toolchain, a deep refactoring of
   the linker, and many fixes in WinIO.

 - Support for multiple home packages, significantly improving support
   in IDEs and other tools for multi-package projects.

 - A refactoring of GHC's error message infrastructure, allowing GHC to
   provide diagnostic information to downstream consumers as structured
   data, greatly easing IDE support.

 - Significant compile-time improvements to runtime and memory consumption.

 - ... and much more

We would like to thank Microsoft Azure, GitHub, IOG, the Zw3rk
stake pool, Tweag I/O, Serokell, Equinix, SimSpace, and other anonymous
contributors whose on-going financial and in-kind support has
facilitated GHC maintenance and release management over the years.
Finally, this release would not have been possible without the hundreds
of open-source contributors whose work comprise this release.

As always, do give this release a try and open a [ticket] if you see
anything amiss.

Happy testing,

- Ben


[GHC proposal 0302]: https://github.com/ghc-proposals/ghc-proposals/blob/master/proposals/0302-cases.rst 
[ticket]: https://gitlab.haskell.org/ghc/ghc/-/issues/new

