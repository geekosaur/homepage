---
author: Zubin Duggal
title: "GHC 9.4.7 is now available"
date: 2023-08-25
tags: release
---

The GHC developers are happy to announce the availability of GHC 9.4.7. Binary
distributions, source distributions, and documentation are available at
[downloads.haskell.org](https://downloads.haskell.org/ghc/9.4.7).

This release is primarily a bugfix release addressing some issues
found in 9.4.7. These include:

 * A bump to bytestring-0.11.5.2 allowing GHC to be bootstrapped on
   systems where the bootstrap compiler is built with the `pthread_condattr_setclock`
   symbol available (#23789).
 * A number of bug fixes for scoping bugs in the specialiser, preventing
   simplifier panics (#21391, #21689, #21828, #23762).
 * Distributing dynamically linked alpine bindists (#23349, #23828).
 * A bug fix for the release notes syntax, allowing them to built on
   systems with older python and sphinx versions (#23807, #23818).
 * ... and a few more. See the [release notes] for a full accounting.

We would like to thank Microsoft Azure, GitHub, IOG, the Zw3rk stake pool,
Well-Typed, Tweag I/O, Serokell, Equinix, SimSpace, Haskell Foundation, and
other anonymous contributors whose on-going financial and in-kind support has
facilitated GHC maintenance and release management over the years. Finally,
this release would not have been possible without the hundreds of open-source
contributors whose work comprise this release.

As always, do give this release a try and open a [ticket][] if you see
anything amiss.

Happy compiling,

- Zubin

[ticket]: https://gitlab.haskell.org/ghc/ghc/-/issues/new
[release notes]: https://downloads.haskell.org/~ghc/9.4.7/docs/users_guide/9.4.7-notes.html
