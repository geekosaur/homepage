---
author: Ben Gamari
title: "GHC 8.4.4 released"
date: 2018-10-14
tags: release
---

The GHC team is pleased to announce the availability of GHC 8.4.4, a
patch-level release in the 8.4 series. The source distribution, binary
distributions, and documentation for this release are available at
[downloads.haskell.org](https://downloads.haskell.org/~ghc/8.4.4).

This release fixes several bugs present in 8.4.3. These include,

  - A bug which could result in memory unsafety with certain uses of
    `touch#` has been resolved. (#14346)

  - A compiler panic triggered by some GADT record updates has been
    fixed (#15499)

  - The `text` library has been updated, fixing several serious bugs in
    the version shipped with GHC 8.4.3 (see `text` issues #227, #221,
    and #197.

  - A serious code generation bug in the LLVM code generation,
    potentially resulting in incorrect evaluation of floating point
    expressions, has been fixed (#14251)

As always, the full release notes can be found in the [users guide](https://downloads.haskell.org/~ghc/8.4.4/docs/html/users_guide/8.4.4-notes.html).    

Thanks to everyone who has contributed to developing, documenting, and
testing this release!

As always, let us know if you encounter trouble.

