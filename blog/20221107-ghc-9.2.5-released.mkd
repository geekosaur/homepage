---
author: Zubin Duggal
title: "GHC 9.2.5 is now available"
date: 2022-11-07
tags: release
---

The GHC developers are happy to announce the availability of GHC 9.2.5. Binary
distributions, source distributions, and documentation are available at
[downloads.haskell.org](https://downloads.haskell.org/ghc/9.2.5).

This release is primarily a bugfix release addressing a few issues
found in 9.2.4. These include:

 * Code generation issues in the AArch64 native code generator backend 
   resulting in incorrect runtime results in some circumstances (#22282, #21964)
 * Fixes for a number of issues with the simplifier leading to core lint
   errors and suboptimal performance (#21694, #21755, #22114)
 * A long-standing interface-file determinism issue where full paths would leak
   into the interface file (#22162)
 * A runtime system bug where creating empty mutable arrays resulted in a crash
   (#21962)
  - ... and a few more. See the [release notes] for a full accounting.

As some of the fixed issues do affect correctness users are encouraged to
upgrade promptly.

We would like to thank Microsoft Azure, GitHub, IOG, the Zw3rk stake pool,
Well-Typed, Tweag I/O, Serokell, Equinix, SimSpace, Haskell Foundation, and
other anonymous contributors whose on-going financial and in-kind support has
facilitated GHC maintenance and release management over the years. Finally,
this release would not have been possible without the hundreds of open-source
contributors whose work comprise this release.

As always, do give this release a try and open a [ticket][] if you see
anything amiss.

Happy compiling,

- Zubin

[ticket]: https://gitlab.haskell.org/ghc/ghc/-/issues/new
[release notes]: https://downloads.haskell.org/~ghc/9.2.5/docs/html/users_guide/9.2.5-notes.html
