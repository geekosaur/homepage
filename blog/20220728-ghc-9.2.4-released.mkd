---
author: Zubin Duggal
title: "GHC 9.2.4 is now available"
date: 2022-07-28
tags: release
---

The GHC developers are very happy to at announce the availability of GHC
9.2.4. Binary distributions, source distributions, and documentation are
available at [`downloads.haskell.org`](https://downloads.haskell.org/ghc/9.2.4).

This release will include:

 - The new `DeepSubsumption` language extension which reverses the
   effects of the [Simplified Subsumption Proposal] introduced in GHC 9.0. This
   is an attempt to make GHC 9.2.4 more backwards compatible with GHC 8.10 and
   eases migration for users who depended on this feature.

   This extension is enabled by default with the `Haskell2010`
   and `Haskell98` languages but disabled with the `GHC2021`
   language originally introduced in GHC 9.2.1.

   See the [Deep Subsumption Proposal] for more details.

 - Fixes for segfaults that may arise due to a bug in the implementation of the
   `keepAlive#` primop. This may regress performance for certain programs which
   use this primop or functions which use the primop, such as `withForeignPtr`.
   These regressions are mostly small, but can be larger in certain edge cases.
   Judicious use of `unsafeWithForeignPtr` when its argument is known not to
   statically diverge can mitigate these in many cases. It is our judgment that
   the critical correctness issues justify the regression in performance and that
   it is important to get a release out with the fix while we work on a better
   approach which will improve performance for future releases (#21708).

   We have a [wiki page](https://gitlab.haskell.org/ghc/ghc/-/wikis/proposal/with-combinator)
   that tracks possible solutions to this problem, and Ben wrote a
   [blog post](https://www.haskell.org/ghc/blog/20210607-the-keepAlive-story.html)
   detailing the introduction of the `keepAlive#` primop and its history.

 - Fixes for a number of miscompilations on AArch64 and other platforms (#21624,
   #21773, #20735, #21685).

 - Fixes for segfaults due to bugs in the RTS and GC (#21708, #21880, #21885).

 - Fixing the behaviour of Ctrl-C with GHCi on Windows (#21889).

  - ... and much more. See the [release notes] for a full accounting.

As some of the fixed issues do affect correctness users are encouraged to
upgrade promptly.

We would like to thank Microsoft Azure, GitHub, IOG, the Zw3rk
stake pool, Tweag I/O, Serokell, Equinix, SimSpace, Haskell Foundation, and other anonymous
contributors whose on-going financial and in-kind support has
facilitated GHC maintenance and release management over the years.
Finally, this release would not have been possible without the hundreds
of open-source contributors whose work comprise this release.

As always, do give this release a try and open a [ticket] if you see
anything amiss.

Happy compiling,

- Zubin

[ticket]: https://gitlab.haskell.org/ghc/ghc/-/issues/new
[release notes]: https://downloads.haskell.org/~ghc/9.2.4/docs/html/users_guide/9.2.4-notes.html
[Simplified Subsumption Proposal]: https://github.com/ghc-proposals/ghc-proposals/blob/master/proposals/0287-simplify-subsumption.rst
[Deep Subsumption Proposal]: https://github.com/ghc-proposals/ghc-proposals/blob/master/proposals/0511-deep-subsumption.rst
