---
author: Bryan Richter
title: "GHC 9.6.3 is now available"
date: 2023-09-25
tags: release
---

The GHC developers are happy to announce the availability of GHC 9.6.3. Binary
distributions, source distributions, and documentation are available on the
[release page](/download_ghc_9_6_3.html).

This release is primarily a bugfix release addressing a few issues
found in the 9.6 series. These include:

* Disable Polymorphic Specialisation (a performance optimisation) by default. It
  was discovered that Polymorphic Specialisation as currently implemented in GHC
  can lead to hard to diagnose bugs resulting in incorrect runtime results.
  Users wishing to use this optimisation despite the caveats will now have to
  explicitly enable the new `-fpolymorphic-specialisation` flag. For more details
  see #23469 as well as #23109, #21229, #23445.

* Improve compile time and code generation performance when `-finfo-table-map` is
  enabled (#23103).
 
* Make the recompilation check more robust when code generation flags are
  changed (#23369).

* Addition of memory barriers that improve soundness on platforms with weak
  memory ordering.

* And dozens of other fixes.

A full accounting of changes can be found in the [release notes]. As
some of the fixed issues do affect correctness users are encouraged to
upgrade promptly.

We would like to thank Microsoft Azure, GitHub, IOG, the Zw3rk stake pool,
Well-Typed, Tweag I/O, Serokell, Equinix, SimSpace, Haskell Foundation, and
other anonymous contributors whose on-going financial and in-kind support has
facilitated GHC maintenance and release management over the years. Finally,
this release would not have been possible without the hundreds of open-source
contributors whose work comprise this release.

As always, do give this release a try and open a [ticket][] if you see
anything amiss.

Enjoy!

-Bryan

[ticket]: https://gitlab.haskell.org/ghc/ghc/-/issues/new
[release notes]: https://downloads.haskell.org/~ghc/9.6.3/docs/users_guide/9.6.3-notes.html
