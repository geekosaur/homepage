{
  description = "GHC homepage";

  inputs.nixpkgs.url = "github:nixos/nixpkgs";

  inputs.flake-utils.url = "github:numtide/flake-utils";

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let pkgs = nixpkgs.legacyPackages.${system}; in
      rec {
        packages = flake-utils.lib.flattenTree {
          ghc-homepage-unwrapped = pkgs.haskellPackages.callCabal2nix "ghc-homepage" ./. {};

          ghc-homepage = pkgs.runCommand "ghc-homepage" {
            nativeBuildInputs = [ pkgs.makeWrapper ];
          } ''
            mkdir -p $out/bin
            makeWrapper \
              "${packages.ghc-homepage-unwrapped}/bin/ghc-homepage" \
              "$out/bin/ghc-homepage" \
              --set LOCALE_ARCHIVE "${pkgs.glibcLocales}/lib/locale/locale-archive" \
              --set LANG en_US.UTF-8
            makeWrapper \
              "${packages.ghc-homepage-unwrapped}/bin/gen-index" \
              "$out/bin/gen-index"
          '';

          check = pkgs.runCommand "check" {
            nativeBuildInputs = [ pkgs.makeWrapper ];
          } ''
            mkdir -p $out/bin
            makeWrapper "${./check.sh}" "$out/bin/check" --prefix PATH : ${pkgs.linkchecker}/bin
          '';
        };
        defaultPackage = packages.ghc-homepage;

        devShells.default = pkgs.mkShell {
          name = "ghc-homepage-utils";
          paths = [
            pkgs.openssh
            pkgs.rclone
            packages.ghc-homepage
            packages.check ];
        };

        apps.ghc-homepage = flake-utils.lib.mkApp { drv = packages.ghc-homepage; };
        apps.gen-index    = flake-utils.lib.mkApp { drv = packages.ghc-homepage; exePath = "/bin/gen-index"; };
        apps.rclone       = flake-utils.lib.mkApp { drv = pkgs.rclone; };
        defaultApp = apps.ghc-homepage;
      }
    );
}
